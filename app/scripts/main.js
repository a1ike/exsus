$(document).ready(function() {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.e-big').slick({
    dots: true,
    customPaging: function(slider, i) {
      var thumb = $(slider.$slides[i]).data();
      return '<a>' + 0 + (i + 1) + '</a>';
    },
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    cssEase: 'linear'
  });

  $('.e-papers__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });

  $('.e-partners__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.e-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  $('.e-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.e-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  $('.e-mob__nav').on('click', function(e) {
    $('.e-mob__more').slideToggle('fast', function(e) {});
  });

  $('.d-service-aside__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__subul').length &&
      !$(e.target).is('.d-service-aside__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__dropsubul_opened');
        });
    }
  });

  $('.d-service-aside__droplinks').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__links').length &&
      !$(e.target).is('.d-service-aside__links')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__droplinks_opened');
        });
    }
  });

  $('.d-prices__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-prices__subul').length &&
      !$(e.target).is('.d-prices__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-prices__dropsubul_opened');
        });
    }
  });

  $('.e-faq__card-header').on('click', function(e) {
    if (
      !$(e.target).closest('.e-faq__card-content').length &&
      !$(e.target).is('.e-faq__card-content')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .toggleClass('e-faq__card-header_opened');
          $(this)
            .prev()
            .parent()
            .toggleClass('e-faq__card_opened');
        });
    }
  });
});
